﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TicTacToeDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int numClicks_ = 0;

        public MainWindow()
        {
            InitializeComponent();
        }


        private void OnClick(object sender, RoutedEventArgs e)
        {


            Button buttonT = (Button)sender;
            if (buttonT.Content.ToString() == "")
            {
                numClicks_++;

                if (numClicks_ % 2 != 0)
                {
                    buttonT.Content = "X";
                }
                else
                {
                    buttonT.Content = "O";
                }
            }

            //Checks for a potential winner
            if (numClicks_ > 4)
            {
                checkWinner();
            }

            //Check if the game is over.
            //If over 4 clicks and under 9 check
            //If clicks == 9 and no Winner, its a draw
        }

        //Checks if there is a winner
        private void checkWinner()
        {
            string symCheck = "O";
            string player = "Player 2";
            if (numClicks_ % 2 != 0)
            {
                symCheck = "X";
                player = "Player 1";
            }

            //Check all columns (if col1 OR col2 OR col3)
            if ( (button.Content == button3.Content && button3.Content == button6.Content && button6.Content.ToString() == symCheck) == true  || 
                 (button1.Content == button4.Content && button4.Content == button7.Content && button7.Content.ToString() == symCheck) == true || 
                 (button2.Content == button5.Content && button5.Content == button8.Content && button8.Content.ToString() == symCheck) == true)
            {
                label.Content = player + " wins!";
                hightlightPath("c");
            }

            //Check all rows (if row1 OR row2 OR row3)
            else if ((button.Content == button1.Content && button1.Content == button2.Content && button2.Content.ToString() == symCheck) == true  ||
                     (button3.Content == button4.Content && button4.Content == button5.Content && button5.Content.ToString() == symCheck) == true ||
                     (button6.Content == button7.Content && button7.Content == button8.Content && button8.Content.ToString() == symCheck) == true)
            {
                label.Content = player + " wins!";
                hightlightPath("r");
            }

            //Check both diagonals
            else if ((button.Content == button4.Content && button4.Content == button8.Content && button8.Content.ToString() == symCheck) == true ||
                    (button2.Content == button4.Content && button4.Content == button6.Content && button6.Content.ToString() == symCheck) == true)
            {
                label.Content = player + " wins!";
                hightlightPath("d");
            }

            else if(numClicks_ == 9)
            {
                label.Content = "DRAW!";
            }
        }

        //Highlights the winning path. Changes the color of buttons to red to show the path
        public void hightlightPath(string direction)
        {
            if(direction == "c")
            {
                if(button.Content == button3.Content && button3.Content == button6.Content)
                {
                    button.Background = Brushes.Red;
                    button3.Background = Brushes.Red;
                    button6.Background = Brushes.Red;
                }
                else if(button1.Content == button4.Content && button4.Content == button7.Content)
                {
                    button1.Background = Brushes.Red;
                    button4.Background = Brushes.Red;
                    button7.Background = Brushes.Red;
                }
                else if (button2.Content == button5.Content && button5.Content == button8.Content)
                {
                    button2.Background = Brushes.Red;
                    button5.Background = Brushes.Red;
                    button8.Background = Brushes.Red;
                }

            }
            else if(direction == "r")
            {
                if (button.Content == button1.Content && button1.Content == button2.Content)
                {
                    button.Background = Brushes.Red;
                    button1.Background = Brushes.Red;
                    button2.Background = Brushes.Red;
                }
                else if (button3.Content == button4.Content && button4.Content == button5.Content)
                {
                    button3.Background = Brushes.Red;
                    button4.Background = Brushes.Red;
                    button5.Background = Brushes.Red;
                }
                else if (button6.Content == button7.Content && button7.Content == button8.Content)
                {
                    button6.Background = Brushes.Red;
                    button7.Background = Brushes.Red;
                    button8.Background = Brushes.Red;
                }
            }
            else if(direction == "d")
            {
                if (button.Content == button4.Content && button4.Content == button8.Content)
                {
                    button.Background = Brushes.Red;
                    button4.Background = Brushes.Red;
                    button8.Background = Brushes.Red;
                }
                else if (button2.Content == button4.Content && button4.Content == button6.Content)
                {
                    button2.Background = Brushes.Red;
                    button4.Background = Brushes.Red;
                    button6.Background = Brushes.Red;
                }
            }
        }

        //Resets the current game
        private void ResetGame(object sender, RoutedEventArgs e)
        {
            numClicks_ = 0;
            button.Content = "";
            button1.Content = "";
            button2.Content = "";
            button3.Content = "";
            button4.Content = "";
            button5.Content = "";
            button6.Content = "";
            button7.Content = "";
            button8.Content = "";
            label.Content = "";
            button.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ffdddddd"));
            button1.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ffdddddd"));
            button2.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ffdddddd"));
            button3.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ffdddddd"));
            button4.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ffdddddd"));
            button5.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ffdddddd"));
            button6.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ffdddddd"));
            button7.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ffdddddd"));
            button8.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ffdddddd"));
        }
    }
}
